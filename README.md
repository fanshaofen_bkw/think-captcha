# fsf/think-captcha

#### 介绍
替换tp6/tp8框架官方扩展库session存储的方式，改为cache缓存的方式继而成为可以api调用的方法

#### 软件架构
tp6、tp8

#### 安装教程

composer require fsf/think-captcha

#### 使用说明

1. 配置文件与官方think-captcha配置参数基本一致，只多了 showCode 配置： true为返回code，false则不返回
2. check($code,$key) 验证验证码 string $code 验证码 string $key 秘钥key
4. create() 获取验证码

#### 参与贡献

1. topthink/think-captcha库
2. fsf提交代码
